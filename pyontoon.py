#pyontoon.py
import random
#import os
import misc
import ascii

class Card:
    def __init__(self, suite, rank):
        self.suite = suite #["Hearts", "Spades", "Diamonds", "Clubs"]
        self.rank = rank #range(0,12)
        self.active = True
        self.iter = 0

    def __iter__(self):
        return self
    
class Player:
    def __init__(self, nick, role):
        self.nick = nick
        self.hand = []
        self.bank = 100
        self.lastBetAmount = 10 #default bet amount
        self.role = role #["Dealer", "Player"]


def cardName(rank, suite):
    names = ["Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"]
    return names[rank] + " of " + suite
    
def genDeck():
    #populates table deck[] with 52 cards, as per a normal deck
    deck = []
    for i in range(13):
        deck = deck + [Card("Hearts", i), Card("Spades", i), Card("Diamonds", i), Card("Clubs", i)]
    global deckSize
    deckSize = len(deck)-1 #for quick reference
    print(deckSize)
    return deck

def genPlayers():   
    players = []
    while True: #exception handling for non-valid type
        try:
            x = int(input("How many players? Min:2\n"))
            if x < 2:
                print("Setting players to default(2)")
                x=2
            break
        except ValueError:
            print("Not a valid number, please try again...")
            
    for i in range(x):
        nick = input("Player " + str(i) +" please choose a name:\n")
        if i==0: players = players + [Player(nick, "Dealer")] #default dealer is player0
        else: players = players + [Player(nick, "Player")]
    return players

def dealCard(pid): #pid refers to location in players[] (int)
    cardRef = random.randint(0,deckSize)
    print(cardRef)
    card = deck.pop(cardRef) #list.pop(x) method removes and returns item x
    players[pid].hand = players[pid].hand + [card]
    
def turn(pid):

    def flip(hand):
        x = 0
        #check if sum of card > 15 (min for flip)
        for i in hand:
            x = x + i.rank + 1
            #print(i.rank)
            #print(x)
        if x < 15:
            print('You only have a score of '+str(x)+' in your hand, minimum is 15')
            return

    def buy(hand,bank):
        print(str(bank)) 

    def sit(x):
        return x
        
    p = players[pid] # N.B. this is NOT a new list
    print(p.nick + ', in your hand you have:')
    for i in p.hand:
        print(cardName(i.rank, i.suite))
    options = {
    "flip":[flip,([p.hand])],
    "buy":[buy, ([p.hand], p.bank)]
    }
    misc.menu(options)



deck = genDeck()
players = genPlayers()
dealCard(1)
dealCard(1)

turn(1)

